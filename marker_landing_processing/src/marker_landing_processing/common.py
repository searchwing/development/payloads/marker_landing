#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
import os

import rospy
import tf
import message_filters
from image_geometry import cameramodels


from std_msgs.msg import Float64, Float64MultiArray
from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from mavros_msgs.msg import Thrust
from geometry_msgs.msg import TwistStamped, PoseStamped, Quaternion, Twist
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from mavros_msgs.srv import SetMode, SetModeRequest

from cv_bridge import CvBridge
cvbridge = CvBridge()

from geometry_msgs.msg import PointStamped



rospy.wait_for_service("/mavros/set_mode")
set_mode_client = rospy.ServiceProxy("/mavros/set_mode", SetMode)


def ros2opencv(img):
    cv_img_rgb = cvbridge.imgmsg_to_cv2(img)
    return cv_img_rgb

def opencv2ros(img,enc):
    ros_img = cvbridge.cv2_to_imgmsg(img, enc)
    return ros_img

def drawText(img,text,position,color=(255, 0, 0),fontScale=2.0,thickness=4):
    # font
    font = cv2.FONT_HERSHEY_SIMPLEX
    # Using cv2.putText() method
    img = cv2.putText(img, text, position, font, 
                    fontScale, color, thickness, cv2.LINE_AA)
    return img

def drawCircles(img,detections,color=(255,0,0)):
    img_rgb = img
    for oneDet in detections:
        #Python: cv.Circle(img, center, radius, color, thickness=1, lineType=8, shift=0) → None
        center = (int(oneDet[0]),int(oneDet[1]))
        cv2.circle(img_rgb ,center,radius=50,color=color,thickness=20)
    return img_rgb

def setUAVMode(mode):
    offb_set_mode = SetModeRequest(0, mode)
    response=set_mode_client(offb_set_mode)
    print(response)

def getMarkerCenterPixel(landingMarker):
    return (landingMarker.Detection2D.bbox.center.x,landingMarker.Detection2D.bbox.center.y)

def get3DRay(Point2D, camModel):
    # get Vector from origin to 3d pos of pixel in the camcoordinatesystem
    # see https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html for visualization
    ray=camModel.projectPixelTo3dRay(Point2D)
    return np.array([ray[0],ray[1],ray[2]])

def genFloat64Msg(data):
    msg=Float64()
    msg.data=float(data)
    return msg

def ptpt3Ddistance(pt0,pt1):
    return np.sqrt(np.power((pt0.x - pt1.x), 2) + np.power((pt0.y - pt1.y), 2) + np.power((pt0.z - pt1.z), 2))

def getPixelFrom3Dpos(Point3D,camModel):
    # get Vector from origin to 3d pos of pixel in the camcoordinatesystem
    # see https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html for visualization
    Point2D = camModel.project3dToPixel((Point3D.x,Point3D.y,Point3D.z))
    return Point2D

# Get Pitch Angle of a pixel with regard to 
def getPixelPitchAngle(Point2D, camModel, stamp,tf_listener):
    camTf="cam"

    tf_listener.waitForTransform(
        "map",
        camTf,
        stamp,
        rospy.Duration(
            0.3
        ),  # wait SHORTER than image periode time otherwise the wait accumulates
    )
 
    drone3dPose = PoseStamped()
    drone3dPose.header.frame_id = "base_link"
    drone3dPose.header.stamp = stamp
    drone3dPoseInMap = tf_listener.transformPose("map", drone3dPose)
    drone3dPos = PointStamped()
    drone3dPos.header = drone3dPose.header
    drone3dPos.point = drone3dPoseInMap.pose.position

    # get Vector from origin to 3d pos of pixel in the camcoordinatesystem
    # see https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html for visualization
    Point3D = camModel.projectPixelTo3dRay(Point2D)

    # generate point in cam coordinate system in ros
    pt = PointStamped()
    pt_transformed = PointStamped()
    pt.point.x = Point3D[0]
    pt.point.y = Point3D[1]
    pt.point.z = Point3D[2]
    pt.header.frame_id = camTf
    pt.header.stamp = stamp

    # transform point to drone coordinate system
    pt_transformed = tf_listener.transformPoint("map", pt)

    # Define ray through pixel in drone coordinate system
    rayPoint = np.array(
        [drone3dPoseInMap.pose.position.x, drone3dPoseInMap.pose.position.y, drone3dPoseInMap.pose.position.z]
    )  # Any point along the ray
    rayDirection = np.array(
        [
            pt_transformed.point.x - drone3dPoseInMap.pose.position.x,
            pt_transformed.point.y - drone3dPoseInMap.pose.position.y,
            pt_transformed.point.z - drone3dPoseInMap.pose.position.z,
        ]
    )
    # Define plane on ground = sea
    planeNormal = np.array([0, 0, 1])
    planePoint = np.array([0, 0, 0])
    angle_plane_to_ray = np.rad2deg(
        np.arccos(np.clip(np.dot(planeNormal, rayDirection), -1.0, 1.0))
    )
    return angle_plane_to_ray

