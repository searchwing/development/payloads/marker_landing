#!/usr/bin/env python

import numpy as np
import os

import rospy
import tf
import message_filters
from image_geometry import cameramodels

from std_msgs.msg import Float64, Float64MultiArray, Header

from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from mavros_msgs.msg import Thrust
from geometry_msgs.msg import TwistStamped, PoseStamped, Quaternion, Twist
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from mavros_msgs.srv import SetMode, SetModeRequest
 

rospy.wait_for_service("/mavros/set_mode")
set_mode_client = rospy.ServiceProxy("/mavros/set_mode", SetMode)


def set_attitude(roll,pitch,yaw,thrust=0.4,header=None):
    if header is None:
        header  = Header()
        header.stamp = rospy.Time.now()
    poseMsg = PoseStamped()
    poseMsg.header = header
    # RPY  
    #positive pitch goes down
    quat = quaternion_from_euler(roll,pitch,yaw)
    poseMsg.pose.orientation.x = quat[0]
    poseMsg.pose.orientation.y = quat[1]
    poseMsg.pose.orientation.z = quat[2]
    poseMsg.pose.orientation.w = quat[3]
    attitude_pub.publish(poseMsg)
    
    thrustMsg = Thrust()
    thrustMsg.header = header
    thrustMsg.thrust = thrust
    thrust_pub.publish(thrustMsg)
  
def callbackAttitude(rollMsg, pitchMsg, yawMsg ):
    set_attitude(rollMsg.data, pitchMsg.data, yawMsg.data)

def callbackRoll(rollMsg ):
    global lastRoll
    global lastPitch
    global lastYaw
    global lastThrust
    set_attitude(rollMsg.data, lastPitch,lastYaw, lastThrust)
    lastRoll=rollMsg.data

def callbackPitch(pitchMsg ):
    global lastRoll
    global lastPitch
    global lastYaw
    global lastThrust
    set_attitude(lastRoll, pitchMsg.data, lastYaw, lastThrust)
    lastPitch=pitchMsg.data

def callbackYaw( yawMsg ):
    global lastRoll
    global lastPitch
    global lastYaw
    global lastThrust
    set_attitude(lastRoll, lastPitch, yawMsg.data, lastThrust)
    lastYaw= yawMsg.data
    
def callbackThrust( thrustMsg ):
    global lastRoll
    global lastPitch
    global lastYaw
    global lastThrust
    set_attitude(lastRoll, lastPitch, lastYaw, thrustMsg.data)
    lastThrust= thrustMsg.data
    
# start node
rospy.init_node('attitude_setter', anonymous=True)

lastRoll=0
lastPitch=0
lastYaw=0
lastThrust=0



thrust_pub = rospy.Publisher('/mavros/setpoint_attitude/thrust', Thrust, queue_size=3)
attitude_pub = rospy.Publisher('/mavros/setpoint_attitude/attitude', PoseStamped, queue_size=3)
twist_pub = rospy.Publisher('/mavros/setpoint_attitude/cmd_vel', TwistStamped, queue_size=3)



# subscriber
pitchInTopicName = rospy.get_param(
    "~i_pitchTopicName", "/attitude/pitch/out"
)
rollInTopicName = rospy.get_param(
    "~i_pitchTopicName", "/attitude/roll/out"
)
yawInTopicName = rospy.get_param(
    "~i_pitchTopicName", "/attitude/yaw/out"
)
thrustInTopicName = rospy.get_param(
    "~i_pitchTopicName", "/attitude/thrust/out"
)
pitch_sub = message_filters.Subscriber(pitchInTopicName, Float64)
roll_sub = message_filters.Subscriber(rollInTopicName, Float64)
yaw_sub = message_filters.Subscriber(yawInTopicName, Float64)
thrust_sub = message_filters.Subscriber(thrustInTopicName, Float64)
pitch_sub.registerCallback(callbackPitch)
roll_sub.registerCallback(callbackRoll)
yaw_sub.registerCallback(callbackYaw)
thrust_sub.registerCallback(callbackThrust)



print("attitude_setter started: Loop until new data arrives")
rospy.spin()


