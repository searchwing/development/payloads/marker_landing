#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
import os

import rospy
import tf
import message_filters
from image_geometry import cameramodels


from std_msgs.msg import Float64, Float64MultiArray
from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from mavros_msgs.msg import Thrust,State
from geometry_msgs.msg import TwistStamped, PoseStamped, Quaternion, Twist
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from mavros_msgs.srv import SetMode, SetModeRequest
from marker_landing_msgs.msg import LandingMarkerArray,LandingMarker,LandingState
from marker_landing_processing.common import *
import copy
import rospy
import tf
import message_filters
from image_geometry import cameramodels
import smach

from std_msgs.msg import Float64, Float64MultiArray
from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from mavros_msgs.msg import Thrust,State
from geometry_msgs.msg import TwistStamped, PoseStamped, Quaternion, Twist
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from mavros_msgs.srv import SetMode, SetModeRequest
from marker_landing_msgs.msg import LandingMarkerArray,LandingMarker,LandingState
#from marker_landing_processing.common import *
import time
import copy
from apriltag_ros.msg import AprilTagDetectionArray, AprilTagDetection


def callbackTags(aprilTagDetectionArrayMsg ):
    global lastDetectedLandingMarker
    global currentUsedLandingMarker

    stamp=aprilTagDetectionArrayMsg.header.stamp
    
    landingMarkerArrayMsg=LandingMarkerArray()
    landingMarkerArrayMsg.header = aprilTagDetectionArrayMsg.header
    
    if len(aprilTagDetectionArrayMsg.detections)>0:
        tf_listener.waitForTransform(
            "map",
            camTfName,
            stamp,
            rospy.Duration(
                0.2
            ),  # wait SHORTER than image periode time otherwise the wait accumulates
        )
        
        drone3dPose = PoseStamped()
        drone3dPose.header.frame_id = "base_link"
        drone3dPose.header.stamp = stamp
        drone3dPoseInMap = tf_listener.transformPose("map", drone3dPose)

        for oneTag in aprilTagDetectionArrayMsg.detections:
            if oneTag.id[0] != targetTagId:
                continue
            tagPoseInCamStamped = PoseStamped()
            tagPoseInCamStamped.header.frame_id = camTfName
            tagPoseInCamStamped.header.stamp = stamp
            tagPoseInCamStamped.pose = oneTag.pose.pose.pose
            tagPoseInMap = tf_listener.transformPose("map", tagPoseInCamStamped)
            distanceCamToTag = ptpt3Ddistance(tagPoseInMap.pose.position,drone3dPoseInMap.pose.position)

            landingTargetInMap = tagPoseInMap
            landingTargetInMap.pose.position.x=landingTargetInMap.pose.position.x+deltaPosMarkerToLandingTargetX
            landingTargetInMap.pose.position.y=landingTargetInMap.pose.position.y+deltaPosMarkerToLandingTargetY
            landingTargetInMap.pose.position.z=landingTargetInMap.pose.position.z+deltaPosMarkerToLandingTargetZ

            landingTargetInCam = tf_listener.transformPose(camTfName, landingTargetInMap)
            landingTargetInCamCenter2D = getPixelFrom3Dpos(landingTargetInCam.pose.position,camModel)
            landingTargetInCamCenter2DPitchAngle=getPixelPitchAngle(landingTargetInCamCenter2D,camModel,stamp,tf_listener)

            oneLandingMarkerMsg=LandingMarker()
            oneLandingMarkerMsg.id = oneTag.id[0]
            oneLandingMarkerMsg.Detection2D=Detection2D()
            oneLandingMarkerMsg.Detection2D.bbox.center.x=float(landingTargetInCamCenter2D[0])
            oneLandingMarkerMsg.Detection2D.bbox.center.y=float(landingTargetInCamCenter2D[1])
            oneLandingMarkerMsg.Detection2D.bbox.size_x=10
            oneLandingMarkerMsg.Detection2D.bbox.size_y=10
            oneLandingMarkerMsg.distanceToCam=distanceCamToTag
            oneLandingMarkerMsg.anglesInWorld=[-1,landingTargetInCamCenter2DPitchAngle,-1] #Roll,Pitch,Yaw
            oneLandingMarkerMsg.ttc=-1
            oneLandingMarkerMsg.header=aprilTagDetectionArrayMsg.header
            oneLandingMarkerMsg.markerPoseInCam=tagPoseInCamStamped
            oneLandingMarkerMsg.markerPoseInMap=landingTargetInMap

            lastDetectedLandingMarker=copy.copy(oneLandingMarkerMsg)

            landingMarkerArrayMsg.markers.append(oneLandingMarkerMsg)
    else:
        lastDetectedLandingMarker=None # reset last detected marker
    landingmarkers_pub.publish(landingMarkerArrayMsg)

    # attitude control loop
    if not currentUsedLandingMarker is None: 
        markerPoseInMap = copy.copy(currentUsedLandingMarker.markerPoseInMap)
        markerPoseInMap.header.stamp=stamp
        landingTargetInCam = tf_listener.transformPose(camTfName, markerPoseInMap)
        landingTargetInCamCenter2D = getPixelFrom3Dpos(landingTargetInCam.pose.position,camModel)
        rayTarget=get3DRay(landingTargetInCamCenter2D,camModel)

        roll_set_pub.publish(genFloat64Msg(rayImgcenter[0]))
        roll_in_pub.publish(genFloat64Msg(rayTarget[0]))

        pixelPitchAngle=getPixelPitchAngle(landingTargetInCamCenter2D,camModel,stamp,tf_listener)
        targetPitchAngle=-np.deg2rad(90-pixelPitchAngle)
        pitch_set_pub.publish(genFloat64Msg(targetPitchAngle))
        
        yaw_set_pub.publish(genFloat64Msg(0))
        if  sm.get_active_states()[0] == 'PRECRASH':
            thrust_set_pub.publish(genFloat64Msg(0.00))
        else:
            thrust_set_pub.publish(genFloat64Msg(0.2))
    else:
        pitch_set_pub.publish(genFloat64Msg(0))
        yaw_set_pub.publish(genFloat64Msg(0))
        roll_set_pub.publish(genFloat64Msg(0))
        roll_in_pub.publish(genFloat64Msg(0))
        thrust_set_pub.publish(genFloat64Msg(0.2))


def callbackState(stateMsg):
    global lastMavState
    lastMavState=copy.copy(stateMsg)

def publishStateMsg(stateName):
    landingStateMsg=LandingState()
    landingStateMsg.header.stamp=rospy.Time.now()
    landingStateMsg.state=stateName[17:-2]
    landingState_pub.publish(landingStateMsg)



# start node
rospy.init_node('landing_marker_controller', anonymous=True)

tf_listener = tf.TransformListener()  # must be outside callback

lastMavState=None
markers=None
lastDetectedLandingMarker=None
currentUsedLandingMarker=None
targetMarkerId=None

# start node 
camTfName = rospy.get_param('~i_camTfName',"cam")
targetTagId  = rospy.get_param('~i_targetTagId',10)
distanceStatePrecrash  = rospy.get_param('~i_distanceStatePrecrash',30)
deltaPosMarkerToLandingTargetX = rospy.get_param('~i_deltaPosMarkerToLandingTargetX',0)
deltaPosMarkerToLandingTargetY = rospy.get_param('~i_deltaPosMarkerToLandingTargetY',0)
deltaPosMarkerToLandingTargetZ = rospy.get_param('~i_deltaPosMarkerToLandingTargetZ',0)

calibTopicName  = rospy.get_param('~i_camCalib',"/rrbot/camera1/camera_info")
calibMsg=rospy.wait_for_message(calibTopicName, CameraInfo, timeout=None)
camModel = cameramodels.PinholeCameraModel()
camModel.fromCameraInfo(calibMsg)
pixelImgCenter=(camModel.width//2,camModel.height//2)
rayImgcenter=get3DRay(pixelImgCenter,camModel)

#publisher
landingmarkers_pub = rospy.Publisher('/landingmarkers/all', LandingMarkerArray, queue_size=3)

roll_set_pub = rospy.Publisher('/attitude/roll/set', Float64, queue_size=3)
pitch_set_pub = rospy.Publisher('/attitude/pitch/out', Float64, queue_size=3)
yaw_set_pub = rospy.Publisher('/attitude/yaw/out', Float64, queue_size=3)
thrust_set_pub = rospy.Publisher('/attitude/thrust/out', Float64, queue_size=3)
roll_in_pub = rospy.Publisher('/attitude/roll/in', Float64, queue_size=3)
pitch_in_pub = rospy.Publisher('/attitude/pitch/in', Float64, queue_size=3)
yaw_in_pub = rospy.Publisher('/attitude/yaw/in', Float64, queue_size=3)

landingMarkerTarget_pub = rospy.Publisher('/landingmarkers/target', LandingMarkerArray, queue_size=3)
landingState_pub = rospy.Publisher('/landingstate', LandingState, queue_size=3)


# subscriber
tagsTopicName = rospy.get_param('~i_tagsTopic',"/tag_detections")
rospy.Subscriber(tagsTopicName ,AprilTagDetectionArray,callbackTags)

stateTopicName  = rospy.get_param('~i_mavModeTopic',"/mavros/state")
rospy.Subscriber(stateTopicName, State,callbackState)
 

# Statemachine
class Approach(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['no_marker_found','marker_found'])

    def execute(self, userdata):
        global lastDetectedLandingMarker
        global currentUsedLandingMarker

        time.sleep(0.1)

        publishStateMsg(str(self.__class__))
        currentUsedLandingMarker=None
        if not lastDetectedLandingMarker is None:
                return 'marker_found'
        else:
            return 'no_marker_found'

class MarkerFound(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['lost_markers','no_user_input','target_marker_set'])

    def execute(self, userdata):
        global lastDetectedLandingMarker
        global currentUsedLandingMarker
        global lastMavState
        time.sleep(0.1)
        publishStateMsg(str(self.__class__))

        if not lastDetectedLandingMarker is None:
            if lastMavState.mode != "GUIDED":
                return 'no_user_input'
            else:
                currentUsedLandingMarker = lastDetectedLandingMarker
                return 'target_marker_set'
        else:
            return 'lost_markers'

class LandingApproach(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['approach_finished','approach_ongoing','approach_abort'])

    def execute(self, userdata):
        global currentUsedLandingMarker
        global lastDetectedLandingMarker
        global distanceStatePrecrash
        global lastMavState
        time.sleep(0.1)
        publishStateMsg(str(self.__class__))

        if lastMavState.mode != "GUIDED":
            return 'approach_abort'
        else:
            if lastDetectedLandingMarker is None:
                currentUsedLandingMarker=currentUsedLandingMarker # curent markers stays last detected marker
            else:
                currentUsedLandingMarker=lastDetectedLandingMarker # update current used marker with meas

            if currentUsedLandingMarker.distanceToCam < distanceStatePrecrash:
                return 'approach_finished'
            else:
                return 'approach_ongoing'

class PreCrash(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['crashed','flare','precrash_abort'])

    def execute(self, userdata):
        global lastMavState
        time.sleep(0.1)
        publishStateMsg(str(self.__class__))

        if lastMavState.mode != "GUIDED":
            return 'precrash_abort'
        else:
            return 'flare'

# Create a SMACH state machine
sm = smach.StateMachine(outcomes=['landed'])

with sm:
    smach.StateMachine.add('APPROACH', Approach(), 
                            transitions={'no_marker_found':'APPROACH', 
                                        'marker_found':'MARKER_FOUND'})
    smach.StateMachine.add('MARKER_FOUND', MarkerFound(), 
                            transitions={'lost_markers':'APPROACH',
                                       'no_user_input':'MARKER_FOUND',
                                       'target_marker_set':'LANDING_APPROACH'})
    smach.StateMachine.add('LANDING_APPROACH', LandingApproach(), 
                            transitions={'approach_finished':'PRE_CRASH',
                                       'approach_ongoing':'LANDING_APPROACH',
                                       'approach_abort':'APPROACH'})
    smach.StateMachine.add('PRE_CRASH', PreCrash(), 
                            transitions={'crashed':'landed',
                                         'precrash_abort':'LANDING_APPROACH',
                                         'flare':'PRE_CRASH'})           

# Execute SMACH plan
outcome = sm.execute()

print("node started: Loop until new data arrives")
rospy.spin()


