#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
import os

import rospy
import tf
import message_filters
from image_geometry import cameramodels


from std_msgs.msg import Float64, Float64MultiArray
from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from mavros_msgs.msg import Thrust
from geometry_msgs.msg import TwistStamped, PoseStamped, Quaternion, Twist
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from mavros_msgs.srv import SetMode, SetModeRequest
from marker_landing_msgs.msg import LandingMarkerArray,LandingMarker,LandingState
from marker_landing_processing.common import *



def drawMarkers(rosImgMsg,landingMarkerArrayMsg):
    global lastLandingStateMsg
    cvImgRGB = ros2opencv(rosImgMsg) 

    cvImg=cvImgRGB
    if not lastLandingStateMsg is None:
        if lastLandingStateMsg.state == "LandingApproach" or lastLandingStateMsg.state == "PreCrash":
            color=(255,0,0)
        else:
            color=(255,255,0)
    else:
        color=(255,255,0)

    #markerpositions in image
    if len(landingMarkerArrayMsg.markers)>0:
        landingMarker=landingMarkerArrayMsg.markers[0]
        centers=[]
        for oneLandingMarker in landingMarkerArrayMsg.markers:
            centers.append(getMarkerCenterPixel(landingMarker))
        cvImg = drawCircles(cvImg,centers,color)

        debugText="dist:{:.0f}".format(landingMarker.distanceToCam)
        debugTextPos=(int(rosImgMsg.width*0.05),int(rosImgMsg.height*0.85))
        cvImg = drawText(cvImg,debugText,debugTextPos,color=color,fontScale=3.0,thickness=7)

    #landingstate
    if not lastLandingStateMsg is None:
        debugTextPos=(int(rosImgMsg.width*0.05),int(rosImgMsg.height*0.95))
        cvImg = drawText(cvImg,"{}".format(lastLandingStateMsg.state),debugTextPos,color=color,fontScale=3.0,thickness=7)

    rosImg = opencv2ros(cvImg,"rgb8")
    return rosImg

def callbackImageAndlandingMarkerArray(rosImgMsg,landingMarkerArrayMsg):
    rosImgWithMarkers=drawMarkers(rosImgMsg,landingMarkerArrayMsg)
    landingMarkersDebugImg_pub.publish(rosImgWithMarkers)
 
def callbackLandingState(landingStateMsg):
    global lastLandingStateMsg
    lastLandingStateMsg=landingStateMsg

# start node
rospy.init_node('debug_image_generator', anonymous=True)

tf_listener = tf.TransformListener()  # must be outside callback


#publisher
landingMarkersDebugImg_pub = rospy.Publisher('/landingmarkers/debug_image', Image, queue_size=3)

# subscriber
imgTopicName  = rospy.get_param('~i_imgTopicName',"/rrbot/camera1/image_raw")
landingMarkerArrayTopicName = rospy.get_param('~i_landingMarkerArrayTopicName',"/landingmarkers/all")
#targetMarkerTopicName = rospy.get_param('~i_targetMarkerTopicName',"/landingmarkers/target")
imageSub = message_filters.Subscriber(imgTopicName, Image)
landingMarkerArraySub = message_filters.Subscriber(landingMarkerArrayTopicName, LandingMarkerArray)
#targetMarkerSub = message_filters.Subscriber(targetMarkerTopicName, LandingMarkerArray)

synced_images = message_filters.TimeSynchronizer([imageSub, landingMarkerArraySub], 3)
synced_images.registerCallback(callbackImageAndlandingMarkerArray)

landingStateTopicName = rospy.get_param('~i_landingStateTopicName',"/landingstate")
landingStateSub = message_filters.Subscriber(landingStateTopicName, LandingState)
landingStateSub.registerCallback(callbackLandingState)
lastLandingStateMsg=None


print("node started: Loop until new data arrives")
rospy.spin()


