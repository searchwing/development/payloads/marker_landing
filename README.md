# marker_landing

## algorithm

* detect marker in image
* calculate pitch angle in world coordinates of the marker center in the image
* set pitch angle as plane pitch
* Use PI-controller on roll to center marker in horizontal direction

## docker

Recommended to use dockerized env from https://github.com/julled/rosdocked.
ros and gazebo is already installed there.

## install simulation

```bash
mkdir extern
cd extern
git clone https://github.com/julled/ardupilot_gazebo-ros1

# apriltags 
git clone https://github.com/koide3/gazebo_apriltag/
```

follow readme of apriltags install. 

## install marker landing 
```bash
cd ..
mkdir -p catkin_ws/src
cd catkin_ws/src
git clone https://gitlab.com/searchwing/development/payloads/marker_landing

git clone -b fixNoetic https://github.com/julled/robotec_pid # PID controller


# from https://github.com/AprilRobotics/apriltag_ros#quickstart
git clone https://github.com/AprilRobotics/apriltag.git      # Clone Apriltag library
git clone https://github.com/AprilRobotics/apriltag_ros.git  # Clone Apriltag ROS wrapper
cd ~/catkin_ws                          # Navigate to the workspace
rosdep install --from-paths src --ignore-src -r -y  # Install any missing packages

catkin build

source devel/setup.zsh
```

## choose marker & camera

Best is to use a small marker which is also visible far away e.g. tag16h5. Print it a reasonable size to be able detect it at least at 150m. Better 250m.
For best camera usage checkout the hardware folder. Keep in mind that e.g. tag16h5 is 8 blocks wide. Therefore at least 8 pixels should be visible at the targeted maximum distance.


## run simulation


* export files

Copy & Paste Followings at the end of .bashrc file

```bash
source /usr/share/gazebo/setup.sh

export GAZEBO_MODEL_PATH=~/ardupilot_gazebo/models:${GAZEBO_MODEL_PATH}
export GAZEBO_MODEL_PATH=~/ardupilot_gazebo/models_gazebo:${GAZEBO_MODEL_PATH}
export GAZEBO_RESOURCE_PATH=~/ardupilot_gazebo/worlds:${GAZEBO_RESOURCE_PATH}
export GAZEBO_PLUGIN_PATH=~/ardupilot_gazebo/build:${GAZEBO_PLUGIN_PATH}
```

* start ArduPlane SITL

```bash
python sim_vehicle.py -w -v ArduPlane -f gazebo-zephyr --map --console
```



* start Gazebo sim

```bash
cd extern/ardupilot_gazebo-ros1
roslaunch launch/plane.launch 
```

## run marker landing

* start in (mavlink) and output (PID) processing
```bash
roslaunch marker_landing_bringup in.launch
```
* start image processing

```bash
rosrun marker_landing_processing processing.py
```

* go to SITL 

```bash
wp load ... # load waypoints
mode auto 
arm throttle
```

switch to vision guided autolanding

```bash
mode guided
```

## debugging

* plotjuggler
* rviz

```bash
rosrun rviz rviz
```


* video streaming

http://wiki.ros.org/web_video_server

```bash
rosrun web_video_server web_video_server
```